<?php

/**
 * @file
 * Hide user signature
 */

/**
 * Module constants.
 */
define('HIDE_USER_SIGNATURE_SHOW', 0);
define('HIDE_USER_SIGNATURE_HIDE', 1);
define('HIDE_USER_SIGNATURE_CUSTOM', 2);

/**
 * Implements hook_schema_alter().
 */
function hide_user_signature_schema_alter(&$schema) {
  $schema['users']['fields']['hide_user_signature'] = array(
    'type' => 'int',
    'size' => 'tiny',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 0,
    'description' => 'Show (0) or hide (1) user signature.',
  );
}

/**
 * Possible values.
 */
function hide_user_signature_get_values($custom = FALSE) {
  $values = array(
    HIDE_USER_SIGNATURE_SHOW => t('Show'),
    HIDE_USER_SIGNATURE_HIDE => t('Hide'),
  );
  if (is_bool($custom) && $custom) {
    $values[HIDE_USER_SIGNATURE_CUSTOM] = t('Custom settings');
  }
  return $values;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function hide_user_signature_form_user_admin_settings_alter(&$form, &$form_state, $form_id) {
  $form['personalization']['hide_user_signature'] = array(
    '#type' => 'radios',
    '#required' => FALSE,
    '#title' => t('User signature visibility'),
    '#description' => t('If %custom selected, users can control the signature visibility in profile page.', array('%custom' => t('Custom settings'))),
    '#default_value' => variable_get('hide_user_signature', HIDE_USER_SIGNATURE_SHOW),
    '#options' => hide_user_signature_get_values(TRUE),
  );
}

/**
 * Implements hook_comment_view_alter().
 */
function hide_user_signature_comment_view_alter(&$build) {
  switch (variable_get('hide_user_signature', HIDE_USER_SIGNATURE_SHOW)) {
    case HIDE_USER_SIGNATURE_SHOW:
      break;

    case HIDE_USER_SIGNATURE_HIDE:
      $build['comment_body']['#object']->signature = '';
      break;

    case HIDE_USER_SIGNATURE_CUSTOM:
      if (isset($GLOBALS['user']->hide_user_signature) && $GLOBALS['user']->hide_user_signature == HIDE_USER_SIGNATURE_HIDE) {
        $build['comment_body']['#object']->signature = '';
      }
      break;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function hide_user_signature_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
  $global_settings = variable_get('hide_user_signature', HIDE_USER_SIGNATURE_SHOW);
  if ($global_settings == HIDE_USER_SIGNATURE_CUSTOM) {
    $form['signature_settings']['hide_user_signature'] = array(
      '#type' => 'radios',
      '#required' => FALSE,
      '#title' => t('User signature visibility'),
      '#default_value' => $form['#user']->hide_user_signature,
      '#options' => hide_user_signature_get_values(),
    );
  }
}

/**
 * Implements hook_views_data_alter().
 */
function hide_user_signature_views_data_alter(&$data) {
  $data['users']['hide_user_signature'] = array(
    'title' => t('Hide user signature'),
    'help' => t('User signature visibility.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Hide user signature'),
      'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
}

/**
 * Implements hook_entity_property_info().
 */
function hide_user_signature_entity_property_info() {
  $info = array();
  $info['user']['properties']['hide_user_signature'] = array(
    'label' => t('Hide user signature'),
    'description' => t('Hide user signature'),
    'type' => 'integer',
    'options list' => 'hide_user_signature_get_values',
  );
  return $info;
}
